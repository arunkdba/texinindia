import { Component, OnInit, OnDestroy } from '@angular/core';
import { trigger, transition, animate, keyframes, style, state, group } from '@angular/animations';
import { Subscription } from 'rxjs';
import { timer, Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { map, filter, scan } from 'rxjs/operators';

@Component({
  selector: 'app-diagonals',
  templateUrl: './diagonals.component.html',
  styleUrls: ['./diagonals.component.css']
})

export class DiagonalsComponent implements OnInit,OnDestroy {

  public showloader: boolean = false;      
  private subscription: Subscription;
  private timer: Observable<any>;

  public ngOnInit() {
    // call this setTimer method when you want to set timer
    // this.setTimer();
  }
  public ngOnDestroy() {
    if ( this.subscription && this.subscription instanceof Subscription) {
      this.subscription.unsubscribe();
    }
  }

  // public setTimer(){

  //   console.log("L0");
  //   // set showloader to true to show loading div on view
  //   this.showloader   = true;
  //   console.log("L1");
  //   this.timer        = Observable.timer(1000); // 5000 millisecond means 5 seconds
  //   console.log("L2");
  //   console.log(this.timer);
  //   this.subscription = this.timer.subscribe(() => {
  //       // set showloader to false to hide loading div from view after 5 seconds
  //       this.showloader = false;
  //   });
  // }

}


