import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatVideoModule } from 'mat-video';
import { AppComponent } from './app.component';
import { ProductManagerComponent } from './product-manager/product-manager.component';
import { EditProductComponent } from './product-manager/edit-product/edit-product.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { FacilitiesComponent } from './facilities/facilities.component';
import { ProductsComponent } from './products/products.component';
import { ValuesComponent } from './values/values.component';
import { ContactusComponent } from './contactus/contactus.component';
import {
                      MatButtonModule,
                      MatFormFieldModule,
                      MatInputModule,
                      MatRippleModule,
                      MatIconModule,
                      MatAutocompleteModule,
                      MatButtonToggleModule,
                      MatCardModule,
                      MatCheckboxModule,
                      MatChipsModule,
                      MatDividerModule,
                      MatExpansionModule,
                      MatGridListModule,
                      MatListModule,
                      MatMenuModule,
                      MatProgressBarModule,
                      MatSidenavModule,
                      MatSliderModule,
                      MatSlideToggleModule,
                      MatSnackBarModule,
                      MatSortModule,
                      MatStepperModule,
                      MatTableModule,
                      MatTabsModule,
                      MatToolbarModule,
                      MatTooltipModule,
                      MatDialogModule,
                      MatDialogRef,
                      
                      MAT_DIALOG_DATA,
                      

} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TopNavComponent } from './top-nav/top-nav.component';
import { MenuItemsComponent } from './menu-items/menu-items.component';
import { AppRoutingModule } from './app-routing.module';
import { NavService } from './nav.service';
import { HospitalsComponent } from './products/hospitals/hospitals.component';
import { HotelsComponent } from './products/hotels/hotels.component';
import { IndustriesComponent } from './products/industries/industries.component';
import { EducationalComponent } from './products/educational/educational.component';
import { MenuComponent } from './menu/menu.component';
import { HomeComponent } from './home/home.component';
import { CorevalueComponent } from './corevalue/corevalue.component';
import { DropDownNavComponent } from './drop-down-nav/drop-down-nav.component';
import { HospitalComponent } from './hospital/hospital.component';
import { GallaryComponent } from './gallary/gallary.component';
import { DiagonalsComponent } from './diagonals/diagonals.component';


@NgModule({
  declarations: [
                    AppComponent,
                    ProductManagerComponent,
                    EditProductComponent,
                    HomeComponent,
                    AboutusComponent,
                    FacilitiesComponent,
                    ProductsComponent,
                    ValuesComponent,
                    ContactusComponent,
                    TopNavComponent,
                    MenuItemsComponent,
                    HospitalsComponent,
                    HotelsComponent,
                    IndustriesComponent,
                    EducationalComponent,
                    MenuComponent,
                    CorevalueComponent,
                    DropDownNavComponent,
                    HospitalComponent,
                    GallaryComponent,
                    DiagonalsComponent,
  ],
  imports: [
                    BrowserModule,
                    MatButtonModule,
                    MatFormFieldModule,
                    MatInputModule,
                    MatRippleModule,
                    BrowserAnimationsModule,
                    MatIconModule,
                    BrowserModule,
                    ReactiveFormsModule,
                    AppRoutingModule,
                    HttpClientModule,
                    MatAutocompleteModule,
                    MatButtonModule,
                    MatButtonToggleModule,
                    MatCardModule,
                    MatCheckboxModule,
                    MatChipsModule,
                    MatDividerModule,
                    MatExpansionModule,
                    MatGridListModule,
                    MatIconModule,
                    MatInputModule,
                    MatListModule,
                    MatMenuModule,
                    MatProgressBarModule,
                    // MatProgressSpinnerModule,
                    // MatRadioModule,
                    // MatRippleModule,
                    // MatSelectModule,
                    MatSidenavModule,
                    MatSliderModule,
                    MatSlideToggleModule,
                    MatSnackBarModule,
                    MatSortModule,
                    MatStepperModule,
                    MatTableModule,
                    MatTabsModule,
                    MatToolbarModule,
                    MatTooltipModule,
                    BrowserAnimationsModule,
                    MatDialogModule,
                    MatVideoModule
                    // MatDialogRef,
                    // AmazingTimePickerModule//,  // For Time Picker
  ],
  providers: [
                    NavService,
                    TopNavComponent,
                    MenuComponent,
                    // HospitalsComponent,
                    //MatDialogRef,
                    {provide:MAT_DIALOG_DATA,useValue:{}},
                    

  ],
  bootstrap: [
                    AppComponent
  ]
})
export class AppModule { }
