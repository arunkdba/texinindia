import { Component, OnInit } from '@angular/core';
import { DropDownNavComponent } from '../drop-down-nav/drop-down-nav.component';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {

  isHome    = true;

  constructor(
                private   dropdown      : DropDownNavComponent
  ) { }

  ngOnInit() {
  }

  move(){
    console.log("Gallary Move");
    this.isHome = !this.isHome;
    this.dropdown.move();
  }

}
