import { Component, OnInit } from '@angular/core';
import { TopNavComponent } from '../top-nav/top-nav.component';
import { NavService } from '../nav.service';
import { MenuComponent } from '../menu/menu.component';
import { DropDownNavComponent } from '../drop-down-nav/drop-down-nav.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  isHome    = true;

  constructor(
                private topNav      : TopNavComponent,
                private menu        : MenuComponent,
                private dropdown    : DropDownNavComponent

  ) { }

  ngOnInit() {
                this.initForm();
  }

  initForm(){
    console.log("Home");
    this.menu.menuClsStatus = false;
    this.menu.menu          = true;
    this.topNav.ngOnInit();
    this.topNav.processHeigh();
    // this.move();
   
  }
  move(){
    console.log("Home Move");
    this.isHome = !this.isHome;
    this.dropdown.move();
  }
  

}
