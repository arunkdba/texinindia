import { NgModule } from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { AboutusComponent } from "./aboutus/aboutus.component";
import { FacilitiesComponent } from "./facilities/facilities.component";
import { ValuesComponent } from "./values/values.component";
import { ContactusComponent } from "./contactus/contactus.component";
import { ProductsComponent } from "./products/products.component";
import { HospitalsComponent } from "./products/hospitals/hospitals.component";
import { HotelsComponent } from "./products/hotels/hotels.component";
import { EducationalComponent } from "./products/educational/educational.component";
import { IndustriesComponent } from "./products/industries/industries.component";
import { CorevalueComponent } from "./corevalue/corevalue.component";
import { GallaryComponent } from "./gallary/gallary.component";


const  appRoutes :Routes=[

                        {path:'home',component:HomeComponent, data: { title: 'Home' },
                        children:[
                            // {path:'products',component:ProductsComponent, data: { title: 'Products' }},
                            {path:'corevalues',component:CorevalueComponent, data: { title: 'CoreValue' }}
                        ]
                        },
                        {path:'aboutus',component:AboutusComponent, data: { title: 'Aboutus' }},
                        {path:'facilities',component:FacilitiesComponent, data: { title: 'Facilities' }},
                        {path:'values',component:ValuesComponent, data: { title: 'Values' }},
                        {path:'contactus',component:ContactusComponent, data: { title: 'Contactus' }},
                        {path:'corevalue',component:CorevalueComponent, data: { title: 'Corevalue' }},
                        
                        {path:'hospital',component:HospitalsComponent, data: { title: 'Hospital' }},
                        {path:'hotel',component:HotelsComponent, data: { title: 'Hotel' }},
                        {path:'edu',component:EducationalComponent, data: { title: 'Educational' }},
                        {path:'indu',component:IndustriesComponent, data: { title: 'Industries' }},
                        {path:'gallary',component:GallaryComponent, data: { title: 'Gallary' }},

                        {path:'products',component:ProductsComponent, data: { title: 'Products' },
                            // children:[
                            //             {path:'hospital',component:HospitalsComponent},
                            //             {path:'hotel',component:HotelsComponent},
                            //             {path:'edu',component:EducationalComponent},
                            //             {path:'indu',component:IndustriesComponent},
                            // ]
                        },
    
    ];

@NgModule({
    imports : [RouterModule.forRoot(appRoutes)],
    exports : [RouterModule]
})

export class AppRoutingModule{
    constructor() { }

  ngOnInit() {
      console.log("Hai");
  }
}