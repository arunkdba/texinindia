import { Component, OnInit } from '@angular/core';
import { DropDownNavComponent } from '../drop-down-nav/drop-down-nav.component';

@Component({
  selector: 'app-gallary',
  templateUrl: './gallary.component.html',
  styleUrls: ['./gallary.component.css']
})
export class GallaryComponent implements OnInit {

  isHome    = true;

  constructor(
                private   dropdown      : DropDownNavComponent
  ) { 
  }

  ngOnInit() {
  }

  move(){
    console.log("Gallary Move");
    this.isHome = !this.isHome;
    this.dropdown.move();
  }

}
