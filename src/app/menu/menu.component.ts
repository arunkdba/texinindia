import { Component, OnInit, ViewChild, ElementRef, VERSION } from '@angular/core';
import { NavService } from '../nav.service';
import { NavItem } from '../top-nav/nav-Item';
import { TopNavComponent } from '../top-nav/top-nav.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],

  // animations : [
  //                document.querySelector('.button').addEventListener('click', () => {
  //                 document.querySelector('.menu__list')
  //                   .classList.toggle('menu__list--animate');
  //               })
  // ]

  
})
export class MenuComponent implements OnInit {

  
    menuStatus      : boolean = false;
    menu            : boolean = false;
    menuClsStatus   : boolean = true;
    
  constructor(
                public navService : NavService,
                public topNav     : TopNavComponent
            ) { }

  ngOnInit() {
    this.initForm();
    // this.topNav.ngOnInit();
    // this.topNav.toggleHeigh();
  }

  initForm(){
    // this.topNav.toggleHeigh();
    this.menuStatus        = true;
    this.menu              = true;
    this.menuClsStatus     = false;
  }

  openTopNav(){
    this.topNav.ngOnInit();
    // this.topNav.toggleHeigh();
    this.navService.openNav();
    this.topNav.h1    = false;
    this.topNav.h2    = false;
    this.topNav.h6    = false;
    this.topNav.logo  = false;
    this.menu         = false;
    this.menuClsStatus= true;

  }

  closeMenu(){
    console.log("CloseMenu");
    this.navService.closeNav();
     this.topNav.h1    = true;
     this.topNav.h2    = true;
     this.topNav.h6    = true;
     this.topNav.logo  = true;
     this.menu         = true;
     this.menuClsStatus= false;
 
   }

}