import { Component, ViewChild, ElementRef, VERSION } from '@angular/core';
import { NavService } from './nav.service';
import { TopNavComponent } from './top-nav/top-nav.component';
import { NavItem } from './top-nav/nav-Item';
import { MenuComponent } from './menu/menu.component';
import { MatDialog } from '@angular/material';
// import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  public topNav                 : boolean  = false;

  constructor(
                private navService       : NavService,
                public  dialog           : MatDialog,
                // public topNav            : TopNavComponent,
                // public menu              : MenuComponent
            ) {
         }

  ngOnInit() {
     console.log("TopNav");
     this.initForm();
  }
      
  public initForm(){
      //this.topNav = true;
  }
  public  hideNav(){
    console.log("hide");
    //this.topNav   = false;
  }
      

//   @ViewChild('appDrawer') appDrawer: ElementRef;

//   version = VERSION;
//   navItems: NavItem[] = [
//     {
//       displayName     : '',
//       iconName        : '',
//     },
//     {
//       displayName     : 'Home',
//       iconName        : 'home',
//       route           : 'home'
//    },
//    {
//       displayName     : 'About us',
//       iconName        : 'aboutus',
//       route           : 'aboutus'
//   },
//   {
//       displayName     : 'Facilities',
//       iconName        : 'facilities',
//       route           : 'facilities'
//  },
//  {
//       displayName     : 'Products',
//       iconName        : 'products',
//       children        : [
//           {
//             displayName : 'Hospitals',
//             iconName    : 'hospital',
//             route       : 'hospital'
//           },
//           {
//             displayName : 'Hotels',
//             iconName    : 'hotel',
//             route       : 'hotel'
//           },
//           {
//             displayName : 'Educational',
//             iconName    : 'edu',
//             route       : 'edu'
//           },
//           {
//             displayName : 'Industries',
//             iconName    : 'indu',
//             route       : 'indu'
//           },
//       ],
//    },
//    {
//         displayName     : 'Values',
//         iconName        : 'values',
//         route           : 'values'

//    },
//    {
//         displayName     : 'Contact us',
//         iconName        : 'contactus',
//         route           : 'contactus'
//    },

// ];



  // ngAfterViewInit() {
  //   console.log(this.appDrawer);
  //   this.navService.appDrawer = this.appDrawer;
  //   console.log(this.appDrawer+"--"+this.navService.appDrawer);
  // }

  // openTopNav(): void {
  //   console.log("Top Nav");
  //   const dialogRef = this.dialog.open(TopNavComponent, {
  //     width: '800px',height:'700px'
  //   });
  // }
}
