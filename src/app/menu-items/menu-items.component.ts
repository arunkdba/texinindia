import { Component, OnInit, HostBinding, Input } from '@angular/core';
import {Router} from '@angular/router';
import { NavService } from '../nav.service';
import { animate, transition, state, trigger, style } from '@angular/animations';
import { NavItem } from '../top-nav/nav-Item';
import { HomeComponent } from '../home/home.component';
import { TopNavComponent } from '../top-nav/top-nav.component';

@Component({
  selector: 'app-menu-items',
  templateUrl: './menu-items.component.html',
  styleUrls: ['./menu-items.component.css'],

  animations: [
       trigger('indicatorRotate', [
       state('collapsed', style({transform: 'rotate(0deg)'})),
       state('expanded', style({transform: 'rotate(180deg)'})),
       transition('expanded <=> collapsed',
       animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
       ),
     ])
    ]

})

export class MenuItemsComponent {

 expanded: boolean;
  @HostBinding('attr.aria-expanded') ariaExpanded = this.expanded;
  @Input() item: NavItem;
  @Input() depth: number;

  constructor(public navService: NavService,
              public router: Router,
              // public topNav   : TopNavComponent
              ) {
    if (this.depth === undefined) {
      this.depth = 0;
    }
  }

  onItemSelected(item: NavItem) {
    // this.topNav.ngOnDestroy();
    if (!item.children || !item.children.length) {
      this.router.navigate([item.route]);
      this.navService.closeNav();
    }
    if (item.children && item.children.length) {
      this.expanded = !this.expanded;
    }
  }

}
