import { Component, OnInit } from '@angular/core';
import { HospitalsComponent } from './hospitals/hospitals.component';
import { DropDownNavComponent } from '../drop-down-nav/drop-down-nav.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  isHome    = true;
  constructor(
                private dropdown    : DropDownNavComponent
              //  private     hospital    : HospitalsComponent,
  ) { 
                //this.hospital.ngOnInit();
  }

  ngOnInit() {
  }

  move(){
    console.log("Products Move");
    this.isHome = !this.isHome;
    this.dropdown.move();
  }

}
