import { Component, OnInit } from '@angular/core';
import { DropDownNavComponent } from 'src/app/drop-down-nav/drop-down-nav.component';

@Component({
  selector: 'app-educational',
  templateUrl: './educational.component.html',
  styleUrls: ['./educational.component.css']
})
export class EducationalComponent implements OnInit {
  isHome    = true;

  constructor(
          private   dropdown      : DropDownNavComponent
  ) { }

  ngOnInit() {
  }

  move(){
    console.log("Gallary Move");
    this.isHome = !this.isHome;
    this.dropdown.move();
  }

}
