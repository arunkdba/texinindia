import { Component, OnInit } from '@angular/core';
import { DropDownNavComponent } from 'src/app/drop-down-nav/drop-down-nav.component';

@Component({
  selector: 'app-hospitals',
  templateUrl: './hospitals.component.html',
  styleUrls: ['./hospitals.component.css']
})
export class HospitalsComponent implements OnInit {

  isHome    = true;

  constructor(
        private   dropdown      : DropDownNavComponent
  ) { }

  ngOnInit() {
    console.log("Hospital");
  }
  
  move(){
    console.log("Gallary Move");
    this.isHome = !this.isHome;
    this.dropdown.move();
  }

}
