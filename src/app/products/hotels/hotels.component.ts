import { Component, OnInit } from '@angular/core';
import { DropDownNavComponent } from 'src/app/drop-down-nav/drop-down-nav.component';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {

  isHome    = true;

  constructor(
          private   dropdown      : DropDownNavComponent
  ) { }

  ngOnInit() {
  }

  move(){
    console.log("Gallary Move");
    this.isHome = !this.isHome;
    this.dropdown.move();
  }

}
