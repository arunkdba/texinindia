import { Component, OnInit } from '@angular/core';
import { DropDownNavComponent } from 'src/app/drop-down-nav/drop-down-nav.component';

@Component({
  selector: 'app-industries',
  templateUrl: './industries.component.html',
  styleUrls: ['./industries.component.css']
})
export class IndustriesComponent implements OnInit {

  isHome    = true;

  constructor(
              private   dropdown      : DropDownNavComponent
  ) { }

  ngOnInit() {
  }

  move(){
    console.log("Gallary Move");
    this.isHome = !this.isHome;
    this.dropdown.move();
  }


}
