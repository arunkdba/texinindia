import { Component, OnInit } from '@angular/core';
import { DropDownNavComponent } from '../drop-down-nav/drop-down-nav.component';

@Component({
  selector: 'app-values',
  templateUrl: './values.component.html',
  styleUrls: ['./values.component.css']
})
export class ValuesComponent implements OnInit {

  isHome    = true;

  constructor(
                private dropdown    : DropDownNavComponent
  ) { }

  ngOnInit() {
                console.log(this.isHome);
  } 

  move(){
                console.log("Value");
                this.isHome = !this.isHome;
                this.dropdown.move();
  }

}
