import {EventEmitter, Injectable} from '@angular/core';
import { TopNavComponent } from './top-nav/top-nav.component';

@Injectable()
export class NavService {
  
  public appDrawer: any;

  constructor(
  ) {
  }

  public closeNav() {
    this.appDrawer.close();
  }

  public openNav() {
    console.log("open");
    this.appDrawer.open();
  }

  public toggleMenu(){
    //this.topNav.toggleHeigh();
  }
  
}
