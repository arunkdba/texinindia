import { Component, OnInit, ViewChild, ElementRef, VERSION, Inject } from '@angular/core';
import { NavService } from '../nav.service';
import { trigger, transition, animate, keyframes, style, state, group } from '@angular/animations';
import { NavItem } from './nav-Item';
import { MenuItemsComponent } from '../menu-items/menu-items.component';
import { MenuComponent } from '../menu/menu.component';
import { MatDialog, MatDialogRef } from '@angular/material';


@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css'],

  // attach this animcation to the element to be animated
  animations: [
    trigger('flyAnimation', [
      // check application of code between enter and leave animations
      transition(':leave', [
        animate('1s', keyframes([                // Animated for 1 second
          style({ opacity: 1, transform: 'translateX(0)', offset: 0 }),   // 0th second element in the actual position
          style({ opacity: 1, transform: 'translateX(-15px)', offset: 0.7 }), // 0.7th second element move -15 pixel from current pos
          style({ opacity: 0, transform: 'translateX(100%)', offset: 1.0 }),
        ])),                      // after showing the element then apply animation
/*         animate('1s', style({
          transform: 'translateX(100%)',
        }))
 */      ]),
      transition(':enter', [
        style({
          transform: 'translateX(-100%)'    // First apply style 
        }),
        animate('1s', keyframes([
          style({ opacity: 0, transform: 'translateX(-100%)', offset: 0 }),
          style({ opacity: 1, transform: 'translateX(15px)', offset: 0.3 }), //  Animated at 0.3 the second
          style({ opacity: 1, transform: 'translateX(0)', offset: 1.0 }),

        ])),                      // after showing the element then apply animation
      ])

      // transition('on <=> off', [animate('2s' , style({transform:'rotate(90deg)'}))])   // <=> usage

    ]),

    trigger('lightsonoff', [                          // switch
      state('off', style({                            // switch in off position
        backgroundColor: 'black'
      })),
      state('on', style({                               // switch in on position. Provide button to toggle
        backgroundColor: 'white'
      })),
      /** From off to on , you can control this transition
       * Do work based on your requirement. Here it will take 2 seconds to transit 
       * animate(transtition time , delay , easing function (check this site easings.net))
       */
      transition('off => on', [
        animate('2s 3s ease-in', style({
          transform: 'rotate(90deg)'
        }))
      ]),
      transition('on => off', [
        animate('2s 1s ease-out', style({
          transform: 'rotate(-90deg)'
        }))
      ])

      // transition('on <=> off', [animate('2s' , style({transform:'rotate(90deg)'}))])   // <=> usage

    ]),
    trigger('groupTrigger', [                          // switch
      state('off', style({                            // switch in off position
        backgroundColor: 'black'
      })),
      state('on', style({                               // switch in on position. Provide button to toggle
        backgroundColor: 'white'
      })),

      transition('on => off', [group([
        animate('4s  ease-in', style({
          transform: 'rotate(90deg)'
        })),
        animate('2s  ease-out', style({
          width: '50px'
        }))
      ])]),
      transition('off => on', [group([
        animate('3s 1s ease-in', style({
          transform: 'rotate(360deg)'
        })),
        animate('4s', style({
          opacity: 0
        }))
      ])])
    ]),

    trigger('heightState', [
      state('noHeight', style({
        height: 0                // sets the size to 0
      })),
      state('fullHeight', style({
        height: '*'              // *  Angular dynamically calculated size
      })),

      transition('fullHeight <=> noHeight', [animate('2s')])
    ])
  ]
})



export class TopNavComponent {

  @ViewChild('appDrawer') appDrawer: ElementRef;
  version = VERSION;
  navItems: NavItem[] = [
    {
      displayName     : '',
      iconName        : '',
    },
    {
      displayName     : 'Home',
      iconName        : 'home',
      route           : 'home'
    },
    {
      displayName     : 'About us',
      iconName        : 'aboutus',
      route           : 'aboutus'
    },
    {
      displayName     : 'Facilities',
      iconName        : 'facilities',
      route           : 'facilities'
    },
  
    {
      displayName     : 'Products',
      iconName        : 'products',
      children        : [
          {
            displayName : 'Hospitals',
            iconName    : 'hospital',
            route       : 'hospital'
          },
          {
            displayName : 'Hotels',
            iconName    : 'hotel',
            route       : 'hotel'
          },
          {
            displayName : 'Educational',
            iconName    : 'edu',
            route       : 'edu'
          },
          {
            displayName : 'Industries',
            iconName    : 'indu',
            route       : 'indu'
          },
       ],
    },
   
    {
        displayName     : 'Contact us',
        iconName        : 'contactus',
        route           : 'contactus'
    },
    {
        displayName     : 'Corevalue',
        iconName        : 'corevalue',
        route           : 'corevalue'
  },

];

  roomstate                   : string = "off";
  heightState                 : string = "fullHeight";
  roomstateparallel           : string = "off";
  showDiv                     : boolean = true;
  companyName                 : string = "Texinindia";

  public closeBtnStatus       : boolean = false;
  public minBtnStatus         : boolean = false;
  public menuStatus           : boolean = false;

  public logo                 : boolean  = false;
  public h1                   : boolean  = false;
  public h2                   : boolean  = false;
  public h6                   : boolean  = false;

  public initialStatus        : boolean  = false;
  public processStatus        : boolean  = false;



  constructor(
                public navService : NavService,
                // public menu       : MenuComponent
             //   public  dialog    : MatDialog,
           //     public dialogRef  : MatDialogRef<TopNavComponent>,
                //@Inject(MAT_DIALOG_DATA) public data: DialogData
    ) { 
     // console.log(this.navItems);
  }
  
  // ngAfterViewInit() {
  //   console.log("L0");
  //   this.navService.appDrawer = this.appDrawer;
  //   console.log(this.appDrawer+"--"+this.navService.appDrawer);
  // }

  ngOnInit() {
    console.log("TopNav");
    this.initForm();
  }

  public initForm(){
    console.log(" TopNav init");
    this.navService.appDrawer = this.appDrawer;
    this.setDisplayButtons(true,true);
    this.menuStatus = true;
    this.logo       = true;
    this.h1         = true;
    this.h2         = true;
    this.h6         = true;
    this.initialStatus = true;

  }

  setDisplayButtons(closeBtnStatus,minBtnStatus){
    console.log("DispBtn");
    console.log(closeBtnStatus+"--"+minBtnStatus);
    this.closeBtnStatus     =  closeBtnStatus;
    this.minBtnStatus       =  minBtnStatus;
  }


  animateFly() {
    this.showDiv = this.showDiv ? false : true;
    console.log(this.showDiv);
  }
  toggleLight() {
    this.roomstate = (this.roomstate === "on") ? "off" : "on";
  }

  toggleLightParallelAnimation() {
    this.roomstateparallel = (this.roomstateparallel === "on") ? "off" : "on";
  }
  public toggleHeigh() {
    console.log("Height-->"+this.heightState);
     this.heightState = (this.heightState === "fullHeight") ? "noHeight" : "fullHeight";
    // this.heightState = (this.heightState === "fullHeight") ? "noHeight" : "fullHeight";
    // console.log(this.heightState);
    // this.setDisplayButtons(false,false);
  }

  minimizeHeigh() {
    this.heightState = (this.heightState === "fullHeight") ? "noHeight" : "fullHeight";
    this.menuStatus  = true;
  }
  public processHeigh(){
    console.log("Process Heigh");
    this.initialStatus    = false;
    this.processStatus    = true;
    console.log(this.processStatus);
  }

  
  // Funcation called before animation start
  aniStarted(event: any) {
    console.log("animation started");
    console.log(event.fromState);
    console.log(event.toState);
    console.log(event.totalTime);
    console.log(event);
  }
  // Funcation called after animation completed
  aniDone(event: any) {
    console.log(event.fromState);
    console.log(event.toState);
    console.log(event.totalTime);
    console.log("animation completed");

  }

  
 

}
