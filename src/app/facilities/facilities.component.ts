import { Component, OnInit } from '@angular/core';
import { DropDownNavComponent } from '../drop-down-nav/drop-down-nav.component';

@Component({
  selector: 'app-facilities',
  templateUrl: './facilities.component.html',
  styleUrls: ['./facilities.component.css']
})
export class FacilitiesComponent implements OnInit {
  isHome    = true;

  constructor(
            private dropdown    : DropDownNavComponent
  ) { }

  ngOnInit() {
  }

  move(){
    console.log("Facilities Move");
    this.isHome = !this.isHome;
    this.dropdown.move();
  }

}
