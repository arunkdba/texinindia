import { Component, OnInit } from '@angular/core';
import { TopNavComponent } from '../top-nav/top-nav.component';
import { DropDownNavComponent } from '../drop-down-nav/drop-down-nav.component';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css']

  // $(function () {
  //   $('.fadein div:gt(0)').hide();
  //   setInterval(function () {
  //     $('.fadein :first-child').fadeOut().next('div').fadeIn().end().appendTo('.fadein');
  //    }, 5000);
  // });

})
export class AboutusComponent implements OnInit {
  isHome    = true;
  constructor(
                private  topNav       : TopNavComponent,
                private dropdown      : DropDownNavComponent
  ) { }

  ngOnInit() {
                this.initForm();
  }

  initForm(){
                console.log("About us");
                this.topNav.ngOnInit();
                this.topNav.processHeigh();
  }

  move(){
                console.log("Aboutus Move");
                this.isHome = !this.isHome;
                this.dropdown.move();
  }
}
