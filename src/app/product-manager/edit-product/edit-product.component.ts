import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, keyframes, group, transition } from '@angular/animations'

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css'],
  // attach this animcation to the element to be animated
  animations: [
    trigger('flyAnimation', [
      // check application of code between enter and leave animations
      transition(':leave', [
        animate('1s', keyframes([                // Animated for 1 second
          style({ opacity: 1, transform: 'translateX(0)', offset: 0 }),   // 0th second element in the actual position
          style({ opacity: 1, transform: 'translateX(-15px)', offset: 0.7 }), // 0.7th second element move -15 pixel from current pos
          style({ opacity: 0, transform: 'translateX(100%)', offset: 1.0 }),
        ])),                      // after showing the element then apply animation
/*         animate('1s', style({
          transform: 'translateX(100%)',
        }))
 */      ]),
      transition(':enter', [
        style({
          transform: 'translateX(-100%)'    // First apply style 
        }),
        animate('1s', keyframes([
          style({ opacity: 0, transform: 'translateX(-100%)', offset: 0 }),
          style({ opacity: 1, transform: 'translateX(15px)', offset: 0.3 }), //  Animated at 0.3 the second
          style({ opacity: 1, transform: 'translateX(0)', offset: 1.0 }),

        ])),                      // after showing the element then apply animation
      ])

      // transition('on <=> off', [animate('2s' , style({transform:'rotate(90deg)'}))])   // <=> usage

    ]),

    trigger('lightsonoff', [                          // switch
      state('off', style({                            // switch in off position
        backgroundColor: 'black'
      })),
      state('on', style({                               // switch in on position. Provide button to toggle
        backgroundColor: 'white'
      })),
      /** From off to on , you can control this transition
       * Do work based on your requirement. Here it will take 2 seconds to transit 
       * animate(transtition time , delay , easing function (check this site easings.net))
       */
      transition('off => on', [
        animate('5s 8s ease-in', style({
          transform: 'rotate(180deg)'
        }))
      ]),
      transition('on => off', [
        animate('2s 5s ease-out', style({
          transform: 'rotate(-270deg)'
        }))
      ])
      // transition('on <=> off', [animate('2s' , style({transform:'rotate(90deg)'}))])   // <=> usage
    ]),

    trigger('groupTrigger', [                          // switch
      state('off', style({                            // switch in off position
        backgroundColor: 'black'
      })),
      state('on', style({                               // switch in on position. Provide button to toggle
        backgroundColor: 'white'
      })),

      transition('on => off', [group([
        animate('4s  ease-in', style({
          transform: 'rotate(90deg)'
        })),
        animate('2s  ease-out', style({
          width: '50px'
        }))
      ])]),
      transition('off => on', [group([
        animate('3s 1s ease-in', style({
          transform: 'rotate(360deg)'
        })),
        animate('4s', style({
          opacity: 0
        }))
      ])])
    ]),
    trigger('heightState', [
      state('noHeight', style({
        height: 0                // sets the size to 0
      })),
      state('fullHeight', style({
        height: 400              // *  Angular dynamically calculated size
      })),
      transition('fullHeight <=> noHeight', [animate('2s')])
    ])
  ]
})

export class EditProductComponent implements OnInit {
  roomstate: string = "off";
  heightState: string = "fullHeight";
  roomstateparallel: string = "off";
  showDiv: boolean = true;
  companyName : string = "Texinindia";

  constructor() { }

  ngOnInit() {

  }

  clearCompanyName(){
    this.companyName    = "";
  }

  animateFly() {
    this.showDiv = this.showDiv ? false : true;
    console.log(this.showDiv);
  }
  toggleLight() {
    this.roomstate = (this.roomstate === "on") ? "off" : "on";
  }

  toggleLightParallelAnimation() {
    this.roomstateparallel = (this.roomstateparallel === "on") ? "off" : "on";
  }
  toggleHeigh() {
    this.heightState = (this.heightState === "fullHeight") ? "noHeight" : "fullHeight";
  }

  closeHeigh() {
    this.heightState =  "noHeight";
  }

  // Funcation called before animation start
  aniStarted(event: any) {
    console.log("animation started");
    console.log(event.fromState);
    console.log(event.toState);
    console.log(event.totalTime);
    console.log(event);
  }
  // Funcation called after animation completed
  aniDone(event: any) {
    console.log(event.fromState);
    console.log(event.toState);
    console.log(event.totalTime);
    console.log("animation completed");

  }

}
