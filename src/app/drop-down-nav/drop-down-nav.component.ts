import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';


@Component({
  selector: 'app-drop-down-nav',
  templateUrl: './drop-down-nav.component.html',
  styleUrls: ['./drop-down-nav.component.css'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        height: '200px',
        opacity: 1,
        backgroundColor: 'yellow',
        transition: 'width 2s, height 4s',
      })),
      state('closed', style({
        height: '100px',
        opacity: 0.5,
        backgroundColor: 'green'
      })),
      transition('open => closed', [
        animate('3s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
      transition('* => closed', [
        animate('1s')
      ]),
      transition('* => open', [
        animate('0.5s')
      ]),
    ]),
    trigger('flyInOut', [
      transition('void => *', [
        style({ transform: ' translateX(-100%) translateY(-100%)'  }),                     // X axis -100       0          100
        animate(1000)
      ]),
      transition('* => void', [
        animate(1000, style({ transform: ' translateY(-100%)' }))       // Above current position negative   below positive
      ])
    ]),
    trigger('flyleftdown', [
      transition('void => *', [
        style({ transform: ' translateX(-100%) translateY(100%)'  }),                     
        animate(1000)
      ]),
      transition('* => void', [
        animate(1000, style({ transform: 'translateX(-100%) translateY(100%)' }))       
      ])
    ]),
    trigger('flyrightdown', [
      transition('void => *', [
        style({ transform: ' translateX(100%) translateY(100%)'  }),                     
        animate(1000)
      ]),
      transition('* => void', [
        animate(1000, style({ transform: ' translateX(100%) translateY(100%)' }))       
      ])
    ]),

    trigger('myInsertRemoveTrigger', [
      transition(':enter', [
        style({ opacity: 0 ,height: '10%'}),
        animate('1s', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('1s', style({ opacity: 0 ,height: '10%' }))
      ])
    ]),
  ],
})

export class DropDownNavComponent implements OnInit {

  constructor(
    
  ) { }

  ngOnInit() {
  }

  title         = 'app';
  isOpen        = false;
  openClose     = "";
  isShown       = true;
  LeftTMove     = true;
  RightTMove    = true;
  isRouter      = false;
  isFirst       = true;

  fly           = "";
  toggle() {
    console.log(this.isOpen);
    this.isOpen = !this.isOpen;
  }
  move(){
    console.log("Drop Down Move");
    this.isShown    = !this.isShown;
    this.LeftTMove  = !this.LeftTMove;
    this.RightTMove = !this.RightTMove;
    this.isRouter   = !this.isRouter;
    this.isFirst    = !this.isFirst;
     console.log(this.isShown);
     console.log(this.isRouter);
     console.log(this.LeftTMove);
  }

}
